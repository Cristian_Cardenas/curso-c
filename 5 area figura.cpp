//encontrar el area de la figura; area semicirculo = pi*r^2/2, area triangulo = base*altura/2.

#include<iostream>

using namespace std;

int main(){
	float area_total = 0, a_semicirculo =0, a_triangulo =0, radio = 10, altura = 13, pi = 3.1416;// definimos variables 
	a_triangulo = (2 * radio * altura)/2; //multiplicamos radio por dos para hallar  la base del triangulo y lo multiplicamos por su altura
	a_semicirculo = pi * (radio * radio)/2;//hallamos el area del circulo completo y lo dividimos por dos para hallar el area del semicirculo	
	area_total = a_semicirculo + a_triangulo;// sumamos las areas
	
	cout<<" El area total de la figura es de: "<<area_total<<" M "<<endl;
	
	return 0;
}
