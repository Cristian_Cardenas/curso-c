//ejemplo de expresiones en c++

#include<iostream>

using namespace std;

int main(){
	float a = 2, b = 4, c = 6, d = 8, e=10, f = 12; //valores de variables
	float resultado = 0;
	
	//operacion A
	resultado = (a/b)+1;
	cout<<"El resultado de la operacion A; (A/B)+1, es igual a "<<resultado<<endl;
	
	//operacion B
	resultado = (a+b)/(c+d);
	cout<<"El resultado de la operacion B; (A+B)/(C+D), es igual a "<<resultado<<endl; 
	
	//operacion C
	resultado = (a+(b/c))/(d+(e/f));
	cout<<"El resultado de la operacion C; A+(B/C)/D+(E/F), es igual a "<<resultado<<endl;
	 
	//operacion D
	resultado = a + (b/(c-d));
	cout<<"el resultado de la operacion D; A+(B/(C-D), es igual a "<<resultado<<endl;
	
	return 0;
}
