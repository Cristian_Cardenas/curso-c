//programa para calcular la nota final media de un alumno

#include<iostream>

using namespace std;

int main(){
	
	// definimos variables.
	float nota_1 = 5.0;
	float nota_2 = 3.0;
	float nota_3 = 2.0;
	float nota_final = 0;
	
	nota_final = (nota_1 + nota_2 + nota_3)/3; //sumamos las notas y las dividimos entre tres.
	
	//mostramos el resultado.
	cout<<" La nota final promedio del alumno es: "<<nota_final<<endl;
	 
	
	return 0;
}
