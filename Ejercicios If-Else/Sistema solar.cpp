//Sistema Solar

#include<iostream>

using namespace std;

int main(){
	
	int seleccion = 0; //definimos la variable donde se va a guardar el dato ingresado.
	
	
	//solicitamos que el usuario ingrese en numero de alguno de los planetas por medio del operador de insercion cout
	
	cout << " Seleccione el numero del planeta para conocer su distancia media al sol"<<endl;
	
	//mostramos en la consola el planeta y el numero asociado a cada planeta. 
	
	cout << " _______________"<<endl;
	cout << "|    Planetas   |"<<endl;
	cout << "|_______________|"<<endl;
	cout << "| 1. | Mercurio |"<<endl;
	cout << "|____|__________|"<<endl;
	cout << "| 2. | Venus    |"<<endl;
	cout << "|____|__________|"<<endl;
	cout << "| 3. | Tierra   |"<<endl;
	cout << "|____|__________|"<<endl;
	cout << "| 4. | Marte    |"<<endl;
	cout << "|____|__________|"<<endl;
	cout << "| 5. | Jupiter  |"<<endl;
	cout << "|____|__________|"<<endl;
	cout << "| 6. | Saturno  |"<<endl;
	cout << "|____|__________|"<<endl;
	cout << "| 7. | Urano    |"<<endl;
	cout << "|____|__________|"<<endl;
	cout << "| 8. | Neptuno  |"<<endl;
	cout << "|____|__________|"<<endl;
	cout << " "<<endl;
	
	//usamos el operador de extraccion cin y lo asignamos para que se guarde en la variable entera seleccion
	cin >> seleccion;
	
	// si la variable es igual al numero del planeta, nos aparece en pantalla la informacion asignada a cada planeta.
	
	if(seleccion ==1){
		
		cout<<seleccion << "."<<" El planeta Mercurio se encuentra a una distancia media de 59 millones de kilometros del sol"<<endl;
	
	}else if(seleccion ==2){
		
		cout<<seleccion << "."<<" El planeta Venus se encuentra a una distancia media de 108 millones de kilometros del sol"<<endl;
		
	}else if(seleccion ==3){
		
		cout<<seleccion << "."<<" El planeta Tierra se encuentra a una distancia media de 150 millones de kilometros del sol"<<endl;
	
	}else if(seleccion ==4){
	
		cout<<seleccion << "."<<" El planeta Marte se encuentra a una distancia media de 228 millones de kilometros del sol"<<endl;
	
	}else if(seleccion ==5){
	
		cout<<seleccion << "."<<" El planeta Jupiter se encuentra a una distancia media de 750 millones de kilometros del sol"<<endl;
	
	}else if(seleccion ==6){
	
		cout<<seleccion << "."<<" El planeta Saturno se encuentra a una distancia media de 1431 millones de kilometros del sol"<<endl;
	
	}else if(seleccion ==7){
	
		cout<<seleccion << "."<<" El planeta Urano se encuentra a una distancia media de 2877 millones de kilometros del sol"<<endl;
	
	}else if(seleccion ==8){
	
		cout<<seleccion << "."<<" El planeta Neptuno se encuentra a una distancia media de 4509 millones de kilometros del sol"<<endl;
		
	}else if(seleccion <=0){ //si la variable seleccion es menor o igual a cero muestra en pantalla el error
		
		cout<<seleccion << "."<<" error seleccione un numero entre 1 y 8"<<endl;
	
	}else if(seleccion >=9){ // si la variable seleccion es mayor o igual a nueve muestra en pantalla el error
		
		cout<<seleccion << "."<<" error seleccione un numero entre 1 y 8"<<endl;
	
	}
	
	return 0;

}
