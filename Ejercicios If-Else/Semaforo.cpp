//Semaforo

#include<iostream>

using namespace std;

int main(){
	
	int seleccion = 0; //definimos la variable donde se va a guardar el dato ingresado.
	
	
	//solicitamos que el usuario ingrese en numero de alguno de los colores por medio del operador de insercion cout
	
	cout << " Seleccione el numero de el color para conocer su significado"<<endl;
	
	//mostramos en la consola el color y el numero asociado a cada uno. 
	
	cout << " _______________"<<endl;
	cout << "|    COLORES    |"<<endl;
	cout << "|_______________|"<<endl;
	cout << "| 1. | Rojo     |"<<endl;
	cout << "|____|__________|"<<endl;
	cout << "| 2. | Verde    |"<<endl;
	cout << "|____|__________|"<<endl;
	cout << "| 3. | Amarillo |"<<endl;
	cout << "|____|__________|"<<endl;
	
	cout << " "<<endl;
	
	//usamos el operador de extraccion cin y lo asignamos para que se guarde en la variable entera seleccion
	cin >> seleccion;
	
	// si la variable es igual al numero del color, nos aparece en pantalla el significado de cada color.
	
	if(seleccion ==1){
		
		cout<<seleccion << "."<<" El Color Rojo Significa No Pasar "<<endl;
	
	}else if(seleccion ==2){
		
		cout<<seleccion << "."<<" El Color Verde Significa Adelante "<<endl;
		
	}else if(seleccion ==3){
		
		cout<<seleccion << "."<<" El Color Amarillo Significa Precaucion"<<endl;
	
	}else if(seleccion <=0){ //si la variable seleccion es menor o igual a cero muestra en pantalla el error
		
		cout<<seleccion << "."<<" error seleccione un numero entre 1 y 3"<<endl;
	
	}else if(seleccion >=4){ // si la variable seleccion es mayor o igual 4 muestra en pantalla el error
		
		cout<<seleccion << "."<<" error seleccione un numero entre 1 y 3"<<endl;
	
	}
	
	return 0;

}
