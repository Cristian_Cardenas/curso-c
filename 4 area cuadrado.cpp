//programa que calcula el area de un cuadrado de lados = 5m el area para un cuadradoesta dada por lado^2.

#include<iostream>

using namespace std;

int main(){
	
	int lado = 5; // definimos variables 
	int area = lado * lado;// multplicamos el lado por el mismo, lo que es igual a lado^2.
	
	cout<<" El area del cuadrado es : "<<area<<" M^2 "<<endl;// mostramos en pantalla el resultado
	 
	return 0;
}
