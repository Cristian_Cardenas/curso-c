//programa para calcular la nota final promediada   de un alumno

#include<iostream>

using namespace std;

int main(){
	
	//definimos variables
	float n_practica = 5.0;
	float n_teorica = 3.0;
	float n_participacion = 2.0;
	float nota_final = 0;
	
	nota_final = (n_practica * 0.30 + n_teorica * 0.60 + n_participacion * 0.10);// multiplicamos cada nota por su respectivo porcentaje y las sumamos.
	
	//mostramos el resultado en pantalla
	cout<<" La nota final promedio del alumno es: "<<nota_final<<endl;
	 
	return 0;
}
