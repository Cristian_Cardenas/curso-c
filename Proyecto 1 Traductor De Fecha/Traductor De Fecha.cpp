//Froyecto 1, Traductor De Fecha

#include<iostream> //libreria de entradas/salidas estandar
#include<clocale> //libreria para poder visualizar caracteres especiales en la consola

using namespace std;

int main(){
	
	setlocale(LC_CTYPE,"spanish");// establece el idioma al espa�ol para poder mostrar caracteres especiales como � y tildes
	
	// definimos las variables a utilizar
	int m;
	int d;
	int a;
	int opc;
	
	// se establece el mensaje por consola donde se pide al usuario que introduzca la fecha
	cout<<"por favor introduzca una fecha, separando (Mes/Dia/A�o) por un espacio:12 22 1993 "<<endl<<endl;
	
	//asignamos los valores ingresados a la variable m, d, y a mes, dia y a�o respectivamente
	cin>>m>>d>>a;
	
	// validamos si las variables han sido ingresads correctamente
	
	if(m<=0){
		
		cout<<"por favor indique un mes valido entre 1 y 12"<<endl;
		
	}else if(d<=0){
		
		cout<<"por favor indique un dia valido entre 1 y 31"<<endl;
		
	}else if(a<1000){
		
		cout<<"por favor indique un a�o valido  "<<endl;
		
	}else if(a>9999){
		
		cout<<"por favor indique un a�o valido  "<<endl;
		
	/* en caso de que las variables sean ingresadas correctamente, desplegamos el men� 
		y solicitamos al usuario que seleccione un tipo de visualizacion
	*/	
	}else{
		
		cout<<endl<<"Seleccione el formato de fecha en el que desea visualizar:"<<m<<"/"<<d<<"/"<<a<<" "<<endl<<endl;
		cout<<"1.  Mes completo, D�a, A�o, (Agosto/12/1993)"<<endl<<endl;
		cout<<"2.  Mes abreviado, D�a, A�o, (Ago/12/1993)"<<endl<<endl;
		cout<<"3.  Numero de Mes, D�a, A�o, (08/12/1993)"<<endl<<endl;
		cout<<"opcion ";cin>>opc;
		cout<<endl;
	
	//validamos la seleccion del usuario y procedemos segun el caso 1 2 o 3
	switch (opc){
		
		case 1:
			if(m==1){
				
				cout<<"La fecha traducida es: enero/"<<d<<"/"<<a<<endl;
				
			}else if(m==2){
				
				cout<<"La fecha traducida es: Febrero/"<<d<<"/"<<a<<endl;
			}else if(m==3){
				
				cout<<"La fecha traducida es: Marzo/"<<d<<"/"<<a<<endl;
			}else if(m==4){
				
				cout<<"La fecha traducida es: Abril/"<<d<<"/"<<a<<endl;
			}else if(m==5){
				
				cout<<"La fecha traducida es: Mayo/"<<d<<"/"<<a<<endl;
			}else if(m==6){
				
				cout<<"La fecha traducida es: Junio/"<<d<<"/"<<a<<endl;
			}else if(m==7){
				
				cout<<"La fecha traducida es: Julio/"<<d<<"/"<<a<<endl;
				
			}else if(m==8){
				
				cout<<"La fecha traducida es: Agosto/"<<d<<"/"<<a<<endl;
			}else if(m==9){
				
				cout<<"La fecha traducida es: Septiembre/"<<d<<"/"<<a<<endl;
			}else if(m==10){
				
				cout<<"La fecha traducida es: Octubre/"<<d<<"/"<<a<<endl;
			}else if(m==11){
				
				cout<<"La fecha traducida es: Noviembre/"<<d<<"/"<<a<<endl;
			}else if(m==12){
				
				cout<<"La fecha traducida es: Diciembre/"<<d<<"/"<<a<<endl;
			}
			break;
			
		case 2:
			
			if(m==1){
				
				cout<<"La fecha traducida es: ene/"<<d<<"/"<<a<<endl;
				
			}else if(m==2){
				
				cout<<"La fecha traducida es: Feb/"<<d<<"/"<<a<<endl;
				
			}else if(m==3){
				
				cout<<"La fecha traducida es: Mar/"<<d<<"/"<<a<<endl;
			
			}else if(m==4){
				
				cout<<"La fecha traducida es: Abr/"<<d<<"/"<<a<<endl;
			
			}else if(m==5){
				
				cout<<"La fecha traducida es: May/"<<d<<"/"<<a<<endl;
			
			}else if(m==6){
				
				cout<<"La fecha traducida es: Jun/"<<d<<"/"<<a<<endl;
			
			}else if(m==7){
				
				cout<<"La fecha traducida es: Jul/"<<d<<"/"<<a<<endl;
				
			}else if(m==8){
				
				cout<<"La fecha traducida es: Ago/"<<d<<"/"<<a<<endl;
			
			}else if(m==9){
				
				cout<<"La fecha traducida es: Sep/"<<d<<"/"<<a<<endl;
			
			}else if(m==10){
				
				cout<<"La fecha traducida es: Oct/"<<d<<"/"<<a<<endl;
			
			}else if(m==11){
				
				cout<<"La fecha traducida es: Nov/"<<d<<"/"<<a<<endl;
			
			}else if(m==12){
				
				cout<<"La fecha traducida es: Dic/"<<d<<"/"<<a<<endl;
			}
			
			break;
			
		case 3:
			
			cout<<"La fecha traducida es:"<<m<<"/"<<d<<"/"<<a<<endl;
			
			break;
			
			default: cout<<endl<<"Seleccion� una opci�n incorrecta"<<endl;/* si el usuario no ingresa un valor entre uno y tres
																			 muestra este mensaje de error */
			
			
	}
	}
	
	cout<<endl<<"Realizado Por: Cristian Cardenas."<<endl;
	
	
		
	return 0;
}

